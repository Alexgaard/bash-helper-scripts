#!/bin/bash
################################################################################
#
# SHORT DESCRIPTION:
# Create and view pdf's.
#
# ORIGINAL AUTHOR:
#   Thomas Alexgaard Jensen
#
# LICENSE:
#   This is free and unencumbered software released into the public domain.
#
#   Anyone is free to copy, modify, publish, use, compile, sell, or
#   distribute this software, either in source code form or as a compiled
#   binary, for any purpose, commercial or non-commercial, and by any
#   means.
#
#   In jurisdictions that recognize copyright laws, the author or authors
#   of this software dedicate any and all copyright interest in the
#   software to the public domain. We make this dedication for the benefit
#   of the public at large and to the detriment of our heirs and
#   successors. We intend this dedication to be an overt act of
#   relinquishment in perpetuity of all present and future rights to this
#   software under copyright law.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
#   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#   OTHER DEALINGS IN THE SOFTWARE.
#
#   For more information, please refer to <http://unlicense.org/>
#
################################################################################

SHOWPDF=0
REQUIREDPROGRAMS=("zathura" "bibtex" "pdflatex")

# Global TEX variables
TEXDATANAMES=(".log" ".out" ".aux" ".bbl" ".bcf" ".blg" ".run.xml" ".tex.bbl" ".tex.blg" ".fls" ".fdb_latexmk" ".toc" ".lot" ".lof" ".nav" ".snm")
TEXDATAPATH="texdata"
BIBCOMPILE=0

# Global MARKDOWN variables
MDDATANAMES=()
MDDATAPATH="mddata"

function help 
{
echo "
LICENSE:
    This is free and unencumbered software released into the public domain.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND.
    Anyone is free to copy, modify, publish, use, compile, sell, or
    distribute this software, either in source code form or as a compiled
    binary, for any purpose, commercial or non-commercial, and by any
    means.
    For more information, please refer to <http://unlicense.org/>

HOW-TO USE:
pdf <FILE>.tex
pdf -s <FILE>.tex

OPTS:
-h      Show this help message.
-b      Use bibtex to compile document, only affects input .tex files.
-s      Open input .tex as .pdf in a viewer.

VIMRC / INIT.VIM IMPLEMENTATION:
Compilation and pdf showing bound to mappings:

autocmd Filetype tex nnoremap <buffer> <LEADER>b <ESC>:!pdf<SPACE>%<CR>
autocmd Filetype tex nnoremap <buffer> <LEADER>r <ESC>:!pdf<SPACE>-s<SPACE>%<CR>

MISSING IMPLEMENTATIONS:
Compilation for markdown.
Automatic detection of pdf viewer.
"
}

function show_pdf 
{
    # open pdf process in shell
    zathura "$1" &
}

function tex_to_pdf 
{
    # Create data dir
    [[ ! -d "$TEXDATAPATH" ]] && mkdir "$TEXDATAPATH"

    # Get basename of input file
    BASENAME=$(basename $1 .tex)

    # If -s specified, show pdf instead of compiling using the -tex filename
    (( "$SHOWPDF" )) && show_pdf "$BASENAME.pdf" && return

    #fetch data files from datadir
    for FILEENDING in "${TEXDATANAMES[@]}"
    do
        [[ -f "$TEXDATAPATH/$BASENAME$FILEENDING" ]] && mv "$TEXDATAPATH/$BASENAME$FILEENDING" "$BASENAME$FILEENDING"
    done

    # https://www.unf.edu/~wkloster/latex/bib.html
    (( $BIBCOMPILE )) && bibtex "$BASENAME" && bibtex "$BASENAME"
    pdflatex "$BASENAME.tex"

    # Return data files to datadir
    for FILEENDING in "${TEXDATANAMES[@]}"
    do
        [[ -f "$BASENAME$FILEENDING" ]] && mv "$BASENAME$FILEENDING" "$TEXDATAPATH/$BASENAME$FILEENDING" 
    done
}

function md_to_pdf 
{
    echo "Markdown compatability not implemented yet!" && exit 1
}

# Check if required programs are present
# https://stackoverflow.com/questions/592620/how-can-i-check-if-a-program-exists-from-a-bash-script
for PROG in "${REQUIREDPROGRAMS[@]}"
do
    [[ ! $(command -v $PROG) ]] && echo "Please install $PROG or manually change pdf viewer." && exit 1
done

# Check for inputs
((!$#)) && echo "No input supplied." && help && exit 1

# Manage opts
while getopts "hbds" opt
do
    case ${opt} in
        h  ) help && exit 0 ;;
        b  ) BIBCOMPILE=1 && shift $((OPTIND-1)) ;;
        s  ) SHOWPDF=1    && shift $((OPTIND-1)) ;;
        \? ) echo "Argument not understood" && help && exit 1 ;;
    esac
done

# Manage input files
for VAR in "$@"
do
    # Check if input file exists
    [[ ! -f "$VAR" ]] && echo "$VAR does not exist." && continue

    case $VAR in
        *.tex ) tex_to_pdf "$VAR" ;;
        *.md  ) md_to_pdf  "$VAR" ;;
        *.pdf ) show_pdf   "$VAR" ;;
    esac
done
